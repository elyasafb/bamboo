class Base(object):
    def fun1(self):
        raise NotImplementedError()

    def fun2(self, a):
        raise NotImplementedError()

    def fun3(self, a, b):
        raise NotImplementedError()

    def fun4(self, a, b, c=None):
        raise NotImplementedError()

    def fun5(self, a, b, *c, d=None, e='fe'):
        raise NotImplementedError()
