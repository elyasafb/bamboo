from bamboo_docstring.EXAMPLES.base import Base


class A(Base):
    def __init__(self):
        pass

    def fun1(self):
        """

        :return:
        """
        pass

    def fun2(self, a):
        """

        :param a:
        :return:
        """
        pass

    def fun3(self, a, b):
        """

        :param a:
        :param b:
        :return:
        """
        pass

    def fun4(self, a, b, c=None):
        """

        :param a:
        :param b:
        :param c:
        :return:
        """
        pass

    def fun5(self, a, b, *c, d=None, e='fe'):
        """


        :param d:
        :param e:
        :return:
        """
        pass
