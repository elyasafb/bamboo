from bamboo_docstring.EXAMPLES.base import Base


class B(Base):
    def fun1(self):
        pass

    def fun2(self, a):
        """

        :return:
        """
        pass

    def fun3(self, a, b):
        pass

    def fun4(self, a, b, c=None):
        pass

    def fun5(self, a, b, *c, d=None, e='fe'):
        pass
