import inspect
import re

PARAMS_NOT_NEED_TO_EXPLAIN = {'self', 'cls', 'exc_type', 'exc_val', 'exc_tb'}


class DocstringMetaData(object):
    def __init__(self, module_obj, module_name, class_obj, class_name, function_obj, function_name):
        self._module_obj = module_obj
        self._module_name = module_name
        self._class_obj = class_obj
        self._class_name = class_name
        self._function_obj = function_obj
        self._function_name = function_name
        self._missing_whole_docstring = False
        self._missing_args_explanation = []

    def _get_function_args(self):
        return inspect.signature(self._function_obj)

    def is_valid_docstring(self):
        args = set(self._get_function_args().parameters) - PARAMS_NOT_NEED_TO_EXPLAIN
        docstring = self._function_obj.__doc__
        if docstring is None:
            self._missing_whole_docstring = True
            return False
        for arg in args:
            pattern = f"\: *param +{arg} *\:"
            if not re.findall(pattern, docstring):
                self._missing_args_explanation.append(arg)
        return len(self._missing_args_explanation) == 0

    def __str__(self):
        explain = None
        if self._missing_args_explanation:
            explain = f'Those args are missing: {self._missing_args_explanation}'
        elif self._missing_whole_docstring:
            explain = 'The whole doc is missing'
        return f'_module_name: {self._module_name}, ' \
               f'_class_name: {self._class_name}, ' \
               f'_function_name: {self._function_name}, ' \
               f'_explain: {explain}, ' \
               f'_doc: {self._function_obj.__doc__}'
