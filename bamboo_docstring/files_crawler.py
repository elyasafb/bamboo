import os


class FilesCrawler(object):
    DELIMITER = '.'
    PY_EXTENSION = 'py'

    def __init__(self, root_directory='..'):
        self._root_directory = root_directory

    def get_py_files_in_repo(self):
        """
        :return: All *.py files under the current directory
        """
        all_files = [f'{dir_path}\\{filename}' for (dir_path, dir_name, file_names) in os.walk(self._root_directory)
                     for filename in
                     file_names]
        py_files = [filename for filename in all_files if self.is_py_file(filename)]
        return py_files

    @classmethod
    def is_py_file(cls, filename):
        if cls.DELIMITER in filename:
            return filename.split('.')[-1].lower() == cls.PY_EXTENSION \
                   and 'bamboo_docstring' not in filename
        return False
