from bamboo_docstring.files_crawler import FilesCrawler
from bamboo_docstring.modules_builder import ModulesBuilder


def _get_all_modules_and_its_paths(py_files_path_lst):
    module_name_and_path = []
    for py_file_path in py_files_path_lst:
        module_name = py_file_path.split('\\')[-1].split('.')[0]
        module_path = py_file_path
        module_name_and_path.append((module_name, module_path))
    return module_name_and_path


class Start(object):
    def __init__(self, root_dir, interface=object):
        self._file_crawler = FilesCrawler(root_dir)
        self._interface = interface

    def start(self):
        py_files_path_lst = self._get_all_python_files_paths()
        module_name_and_path = _get_all_modules_and_its_paths(py_files_path_lst)
        module_objects_lst = self._get_all_modules_objects(module_name_and_path)
        return self._validate_docstring_in_modules(module_objects_lst)

    def _get_all_python_files_paths(self):
        return self._file_crawler.get_py_files_in_repo()

    def _get_all_modules_objects(self, module_name_and_path):
        modules_objects_lst = []
        for module_name, module_path in module_name_and_path:
            module_obj = ModulesBuilder(module_path, module_name, interface_to_implement=self._interface)
            modules_objects_lst.append(module_obj)
        return modules_objects_lst

    @staticmethod
    def _validate_docstring_in_modules(module_objects_lst):
        is_valid = True
        for module_obj in module_objects_lst:
            docstring_obj_lst = module_obj.get_all_docstring_meta_data()
            for docstring_obj in docstring_obj_lst:
                if not docstring_obj.is_valid_docstring():
                    is_valid = False
                    print(docstring_obj)
        return is_valid
