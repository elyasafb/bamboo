import importlib.util
import inspect

from bamboo_docstring.config import INTERFACE_DICT
from bamboo_docstring.docstring_meta_data import DocstringMetaData


class ModulesBuilder(object):

    def __init__(self, module_path, module_name, *functions_name, interface_to_implement=object):
        self._module_path = module_path
        self._module_name = module_name
        self._functions_name = functions_name
        self._interface_to_implement = interface_to_implement
        self._function_to_select = [] if interface_to_implement is object else INTERFACE_DICT[interface_to_implement]

    def get_all_docstring_meta_data(self):
        docstring_meta = []
        module = self._load_module()
        classes = self._get_module_classes(module)
        for class_obj, class_name in classes:
            for function_name, function_obj in self._get_class_functions_name(class_obj):
                if (self._function_to_select and function_name in self._function_to_select) or \
                        not self._function_to_select:
                    docstr = DocstringMetaData(module, self._module_name, class_obj, class_name, function_obj,
                                               function_name)
                    docstring_meta.append(docstr)
        return docstring_meta

    def _load_module(self):
        spec = importlib.util.spec_from_file_location(self._module_name, self._module_path)
        module = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(module)
        return module

    @staticmethod
    def _get_class_functions_name(class_obj):
        """
        :param class_obj: class object for example  DocstringFinder, not DocstringFinder()!!!!
        :return: [(function_name , function_obj),...] so you can explore function_obj like that function_obj.__doc__
        """

        def predicate(obj):
            return inspect.isfunction(obj) or inspect.ismethod(obj)

        return inspect.getmembers(class_obj, predicate=predicate)

    def _get_module_classes(self, module):
        """
        :param module: module object
        :return: [(class_obj , class_name),...]
        """
        classes = []
        for name, obj in inspect.getmembers(module):
            if inspect.isclass(obj) and issubclass(obj,
                                                   self._interface_to_implement) and obj.__module__ == self._module_name:
                classes.append((obj, obj.__name__))
        return classes
