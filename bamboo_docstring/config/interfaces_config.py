from bamboo_docstring.EXAMPLES.base import Base
from bamboo_docstring.EXAMPLES.demo import Demo

_DEMO_INTERFACE = Demo
_DEMO_FUNCTION = ['fun', 'fun2']
_DEMO_NAME = 'demo'

_BASE_INTERFACE = Base
_BASE_FUN = [f'fun{x}' for x in range(1, 3)]
_BASE_NAME = 'base'

"""
How to add new interface:

_<NAME>_INTERFACE = <Interface>
_<NAME>_FUNCTION = ['fun', 'fun2']
_<NAME>_NAME = 'name'

add _<NAME>_INTERFACE  to _INTERFACES_LST
add _<NAME>_FUNCTION  to _INTERFACES_FUN_LST
add _<NAME>_NAME  to _INTERFACES_NAME_LST

** The order is important **
"""

_INTERFACES_LST = [_DEMO_INTERFACE, _BASE_INTERFACE]
_INTERFACES_FUN_LST = [_DEMO_FUNCTION, _BASE_FUN]
_INTERFACES_NAME_LST = [_DEMO_NAME, _BASE_NAME]
INTERFACE_DICT = dict(zip(_INTERFACES_LST, _INTERFACES_FUN_LST))

STRING_TO_CLASS = {**{'object': object}, **dict(zip(_INTERFACES_NAME_LST, _INTERFACES_LST))}
