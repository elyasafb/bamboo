---
##Docstring Checker

---

#example:



Let's say we have this abstract Base class:  

    class Base(object):
        def fun1(self):
            raise NotImplementedError()
    
        def fun2(self, a):
            raise NotImplementedError()


And also we have A,B classes that derived from Base class:

- Class A - in module a
   
      from EXAMPLES.base import Base
      
      
      
       class A(Base):  
                def fun1(self):
            
                """
        
                :return:
                """
                pass
        
            def fun2(self, a):
                """
        
                :param a:
                :return:
                """
                pass
                
---                

- Class B - in module b
   
      from EXAMPLES.base import Base
      
      
      
      class B(Base):
            def fun1(self):
                pass
        
            def fun2(self, a):
                pass

---
In class A we have docstring in both function fun1, fun2 but in class B we haven't so our output should look like this:
    
    _module_name: b, _class_name: B, _function_name: fun1, _explain: The whole doc is missing, _doc: None
    _module_name: b, _class_name: B, _function_name: fun2, _explain: The whole doc is missing, _doc: None
    
Lets modify fun2 in class B:
    
    def fun2(self, a):
    """
    :return:
    """
    pass
    
Now we have docstring but, its missing *:param a:* so our output should look like this:

    _module_name: b, _class_name: B, _function_name: fun1, _explain: The whole doc is missing, _doc: None
    _module_name: b, _class_name: B, _function_name: fun2, _explain: Those args are missing: ['a'], _doc:

---

#HOW TO:

--- 

1. Usage:      
    
        main.py [-h] -rd ROOT_DIRECTORY [-i {demo,base,object}]

2. Optional arguments:
        
       -h, --help            show this help message and exit
       -rd ROOT_DIRECTORY, --root_directory ROOT_DIRECTORY
                            \path\to\root\directory\of\the\repo
       -i {demo,base,object}, --interface {demo,base,object}
                              interface that the classes you want to validate are
                              implemented it
  
3. Assumption:

    The interface should be Class from libs and not from the current project, for example: 
    object is good and  Demo class, from the example section, is bad...  
    
4. Add new interface:
        
   Go to config.interfaces_config.py and follow the instructions.
  