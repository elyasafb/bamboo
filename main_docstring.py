import argparse

from bamboo_docstring.integ import Start
from bamboo_docstring.config import STRING_TO_CLASS


def init_args_parse():
    parser = argparse.ArgumentParser(
        description='sum the integers at the command line')
    parser.add_argument('-rd', '--root_directory', help='\\path\\to\\root\\directory\\of\\the\\repo', default='..')
    parser.add_argument('-i', '--interface', help='interface that the classes you want to validate are implemented it',
                        choices=list(STRING_TO_CLASS.keys()), default='object')
    return parser.parse_args()


def parse_args(args):
    _interface = STRING_TO_CLASS[args.interface]
    _root_directory = args.root_directory
    return _root_directory, _interface


if __name__ == '__main__':
    args = init_args_parse()
    root_directory, interface = parse_args(args)
    Start(root_directory, interface).start()
